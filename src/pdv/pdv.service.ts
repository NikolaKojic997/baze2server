import { Injectable } from '@nestjs/common';
import { dbBrocker } from 'src/db/db-brocker';
import { Pdv } from './pdv';

@Injectable()
export class PdvService {
    async getAll() {
        let res = await dbBrocker.getInstance().executeQuery('SELECT * FROM PDV');
        let arr: Pdv[] = [];
        for (let element of res.rows) {
            let p: Pdv = {
                sifraPDV: element[0],
                iznosPDV: element[1]
            }
            arr.push(p);
        }
        return arr;
    }
}

import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { SkladisteModule } from './skladiste/skladiste.module';
import { MaloprodajaModule } from './maloprodaja/maloprodaja.module';
import { OtpremaModule } from './otprema/otprema.module';
import { OtpremnicaModule } from './otpremnica/otpremnica.module';
import { StavkaModule } from './stavka/stavka.module';
import { ProizvodModule } from './proizvod/proizvod.module';
import { PdvModule } from './pdv/pdv.module';
import { KalkulacijaModule } from './kalkulacija/kalkulacija.module';
import { DobavljacModule } from './dobavljac/dobavljac.module';

@Module({
  imports: [SkladisteModule, MaloprodajaModule, OtpremaModule, OtpremnicaModule, StavkaModule, ProizvodModule, PdvModule, KalkulacijaModule, DobavljacModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}

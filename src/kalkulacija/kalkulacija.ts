export type Kalkulacija = {
    sifraKalkulacije: number,
    datumDPO: Date,
    datumPrijemaDobra: Date,
    datumValute: Date,
    datumDokumenta: Date,
    sifraDobavljaca: number,
    sifraSkladista: number
}
import { Module } from '@nestjs/common';
import { KalkulacijaController } from './kalkulacija.controller';
import { KalkulacijaService } from './kalkulacija.service';

@Module({
  imports: [],
  controllers: [KalkulacijaController],
  providers: [KalkulacijaService],
  exports: [KalkulacijaService]
})
export class KalkulacijaModule {}

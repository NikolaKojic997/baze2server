import { Injectable } from '@nestjs/common';
import { dbBrocker } from 'src/db/db-brocker';
import { Kalkulacija } from './kalkulacija';
const date = require('date-and-time');

@Injectable()
export class KalkulacijaService {
    async getAll(): Promise<Kalkulacija[]> {
        let res = await dbBrocker.getInstance().executeQuery(
            "SELECT * FROM KALKULACIJA_CENE_VIEW"
        );
        console.log(res);
        let arr: Kalkulacija[] = [];
        for (let element of res.rows) {
            let k: Kalkulacija = {
                sifraKalkulacije: element[0],
                datumDPO: new Date(element[1]),
                datumPrijemaDobra: new Date(element[2]),
                datumValute: new Date(element[3]),
                datumDokumenta: new Date(element[4]),
                sifraDobavljaca: element[5],
                sifraSkladista: element[6]
            };
            arr.push(k);
        }
        return arr;
    }

    async getOne(id: number): Promise<Kalkulacija> {
        let res = await dbBrocker.getInstance().executeQuery(
            `SELECT * FROM KALKULACIJA_CENE_VIEW WHERE SIFRA_KALKULACIJE_CENE = ${id}`
        );
        console.log(res);
        
            let k: Kalkulacija = {
                sifraKalkulacije: res.rows[0][0],
                datumDPO: new Date(res.rows[0][1]),
                datumPrijemaDobra: new Date(res.rows[0][2]),
                datumValute: new Date(res.rows[0][3]),
                datumDokumenta: new Date(res.rows[0][4]),
                sifraDobavljaca: res.rows[0][5],
                sifraSkladista: res.rows[0][6]
            };
            return k;
        
    }

    async delete(id: number) {
       return await dbBrocker.getInstance().executeQuery(`DELETE FROM KALKULACIJA_CENE_VIEW WHERE SIFRA_KALKULACIJE_CENE = ${id}`);
    }

    async insert(k: Kalkulacija) {
        return await dbBrocker.getInstance().executeQuery(`insert into KALKULACIJA_CENE_VIEW 
        values (${k.sifraKalkulacije},'${date.format(new Date(k.datumDPO), 'DD-MMM-YYYY')}', '${date.format(new Date(k.datumPrijemaDobra), 'DD-MMM-YYYY')}', '${date.format(new Date(k.datumValute), 'DD-MMM-YYYY')}','${date.format(new Date(k.datumDokumenta), 'DD-MMM-YYYY')}',${k.sifraDobavljaca} ,${k.sifraSkladista})`);
     }

     async update(k: Kalkulacija){
        return await dbBrocker.getInstance().executeQuery(`UPDATE KALKULACIJA_CENE_VIEW 
        SET DATUM_DPO = '${date.format(new Date(k.datumDPO), 'DD-MMM-YYYY')}',DATUM_PRIJEMA_DOBRA = '${date.format(new Date(k.datumPrijemaDobra), 'DD-MMM-YYYY')}',DATUM_VALUTE= '${date.format(new Date(k.datumValute), 'DD-MMM-YYYY')}',DATUM_DOKUMENTA = '${date.format(new Date(k.datumDokumenta), 'DD-MMM-YYYY')}',SIFRA_DOBAVLJACA = ${k.sifraDobavljaca} ,SIFRA_SKLADISTA = ${k.sifraSkladista}
        WHERE SIFRA_KALKULACIJE_CENE = ${k.sifraKalkulacije}`);
     }


}

import { Test, TestingModule } from '@nestjs/testing';
import { KalkulacijaService } from './kalkulacija.service';

describe('KalkulacijaService', () => {
  let service: KalkulacijaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [KalkulacijaService],
    }).compile();

    service = module.get<KalkulacijaService>(KalkulacijaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

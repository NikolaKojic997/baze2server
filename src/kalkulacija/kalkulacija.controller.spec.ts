import { Test, TestingModule } from '@nestjs/testing';
import { KalkulacijaController } from './kalkulacija.controller';

describe('KalkulacijaController', () => {
  let controller: KalkulacijaController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [KalkulacijaController],
    }).compile();

    controller = module.get<KalkulacijaController>(KalkulacijaController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

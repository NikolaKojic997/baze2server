import { Body, Controller, Delete, Get, Param, Post ,Put} from '@nestjs/common';
import { Kalkulacija } from './kalkulacija';
import { KalkulacijaService } from './kalkulacija.service';

@Controller('kalkulacija')
export class KalkulacijaController {
    constructor(private readonly kalkulacijaService : KalkulacijaService){}

    @Get()
    async getAll(){
        return await this.kalkulacijaService.getAll();
    }

    @Get(':id')
    async getOne(@Param('id') id : number){
        return await this.kalkulacijaService.getOne(id);
    }

    @Delete(':id')
    async delete(@Param('id') id : number){
        return await this.kalkulacijaService.delete(id);
    }

    @Post()
    async insert(@Body() k : Kalkulacija){
        return await this.kalkulacijaService.insert(k);
    }

    @Put()
    async update(@Body() k : Kalkulacija){
        return await this.kalkulacijaService.update(k);
    }
}

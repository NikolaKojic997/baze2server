import { Injectable } from '@nestjs/common';
import { dbBrocker } from 'src/db/db-brocker';
import { Otprema } from './otprema';

@Injectable()
export class OtpremaService {
    async getAllOtprema(): Promise<Otprema[]> {
       let res = await dbBrocker.getInstance().executeQuery(
           'SELECT * FROM OTPREMA'
       )
       let arr: Otprema[] = [];
        for (let element of res.rows) {
            let s: Otprema = {
                sifraOtpreme: element[0]
            };
            arr.push(s);
        }
        return arr;

    }
}

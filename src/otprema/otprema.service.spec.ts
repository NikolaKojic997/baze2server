import { Test, TestingModule } from '@nestjs/testing';
import { OtpremaService } from './otprema.service';

describe('OtpremaService', () => {
  let service: OtpremaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [OtpremaService],
    }).compile();

    service = module.get<OtpremaService>(OtpremaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

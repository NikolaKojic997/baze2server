import { Controller, Get } from '@nestjs/common';
import { OtpremaService } from './otprema.service';

@Controller('otprema')
export class OtpremaController {
    constructor(private readonly otpremaService : OtpremaService){

    }
    @Get()
    async getAll(){
       return await this.otpremaService.getAllOtprema();
    }
}

import { Test, TestingModule } from '@nestjs/testing';
import { OtpremaController } from './otprema.controller';

describe('OtpremaController', () => {
  let controller: OtpremaController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [OtpremaController],
    }).compile();

    controller = module.get<OtpremaController>(OtpremaController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

import { OtpremaController } from "./otprema.controller";
import { OtpremaService } from "./otprema.service";
import { Module } from '@nestjs/common';

@Module({
  imports: [],
  controllers: [OtpremaController],
  providers: [OtpremaService],
  exports: [OtpremaService]
})
export class OtpremaModule {}

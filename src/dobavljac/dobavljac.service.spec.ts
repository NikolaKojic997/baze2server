import { Test, TestingModule } from '@nestjs/testing';
import { DobavljacService } from './dobavljac.service';

describe('DobavljacService', () => {
  let service: DobavljacService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DobavljacService],
    }).compile();

    service = module.get<DobavljacService>(DobavljacService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

import { Injectable } from '@nestjs/common';
import { dbBrocker } from 'src/db/db-brocker';
import { Dobavljac } from './dobavljac';

@Injectable()
export class DobavljacService {
    async getAll() : Promise<Dobavljac[]> {
        let res = await dbBrocker.getInstance().executeQuery(
            "SELECT * FROM DOBAVLJAC"
        );
        console.log(res);
        let arr: Dobavljac[] = [];
        for (let element of res.rows) {
            let d: Dobavljac = {
               sifraDobavljaca: element[0],
               nazivDobavljaca: element[1]
            };
            arr.push(d);
        }
        return arr;
    }

    async getOne(id: number) : Promise<Dobavljac> {
        let res = await dbBrocker.getInstance().executeQuery(
            `SELECT * FROM DOBAVLJAC where SIFRA_DOBAVLJACA = ${id}`
        );
        
            let d: Dobavljac = {
               sifraDobavljaca: res.rows[0][0],
               nazivDobavljaca: res.rows[0][1]
            };
        
        return d;
    }

    
}

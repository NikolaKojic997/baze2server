import { Module } from '@nestjs/common';
import { DobavljacController } from './dobavljac.controller';
import { DobavljacService } from './dobavljac.service';

@Module({
  imports: [],
  controllers: [DobavljacController],
  providers: [DobavljacService],
  exports: [DobavljacService]
})
export class DobavljacModule {}

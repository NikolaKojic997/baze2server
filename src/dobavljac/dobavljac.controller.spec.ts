import { Test, TestingModule } from '@nestjs/testing';
import { DobavljacController } from './dobavljac.controller';

describe('DobavljacController', () => {
  let controller: DobavljacController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DobavljacController],
    }).compile();

    controller = module.get<DobavljacController>(DobavljacController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

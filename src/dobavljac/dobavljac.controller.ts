import { Controller, Get } from '@nestjs/common';
import { DobavljacService } from './dobavljac.service';

@Controller('dobavljac')
export class DobavljacController {
    constructor(private readonly dobavljacService : DobavljacService){

    }
    @Get()
    async GetAll(){
        return await this.dobavljacService.getAll();
    }
}

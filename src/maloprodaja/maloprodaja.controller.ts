import { Controller, Get } from '@nestjs/common';
import { MaloprodajaService } from './maloprodaja.service';

@Controller('maloprodaja')
export class MaloprodajaController {
    constructor(private readonly maloprodajaService: MaloprodajaService){}

    @Get()
    async getAllMaloprodaja(){
        return await this.maloprodajaService.getAllMaloprodaja();
    }
}

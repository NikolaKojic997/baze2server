import { Test, TestingModule } from '@nestjs/testing';
import { MaloprodajaService } from './maloprodaja.service';

describe('MaloprodajaService', () => {
  let service: MaloprodajaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MaloprodajaService],
    }).compile();

    service = module.get<MaloprodajaService>(MaloprodajaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

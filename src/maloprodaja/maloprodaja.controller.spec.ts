import { Test, TestingModule } from '@nestjs/testing';
import { MaloprodajaController } from './maloprodaja.controller';

describe('MaloprodajaController', () => {
  let controller: MaloprodajaController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MaloprodajaController],
    }).compile();

    controller = module.get<MaloprodajaController>(MaloprodajaController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

import { Injectable } from '@nestjs/common';
import { dbBrocker } from 'src/db/db-brocker';
import { Maloprodaja } from './maloprodaja';

@Injectable()
export class MaloprodajaService {
    async getAllMaloprodaja(): Promise<Maloprodaja[]> {
        {
            let res = await dbBrocker.getInstance().executeQuery(
                "SELECT * FROM MALOPRODAJA"
            );
            let arr: Maloprodaja[] = [];
            for (let element of res.rows) {
                let m: Maloprodaja = {
                    imeMaloprodaje: element[1],
                    sifraMaloprodaje: element[0]
                };
                arr.push(m);
            }
            return arr;
        }
    }
}

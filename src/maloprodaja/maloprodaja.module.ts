import { Module } from '@nestjs/common';
import { MaloprodajaController } from './maloprodaja.controller';
import { MaloprodajaService } from './maloprodaja.service';


@Module({
  imports: [],
  controllers: [MaloprodajaController],
  providers: [MaloprodajaService],
  exports: [MaloprodajaService]
})
export class MaloprodajaModule {}

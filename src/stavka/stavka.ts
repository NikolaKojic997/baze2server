export type Stavka = {
    redniBroj?: number,
    sifraOtpremnice?: number,
    datumDokumenta?: Date,
    proizvod?: string,
    kolicina?: number,
    cena?: number,
    pdv?: number,
    ukupno?: number,
    rabat?: number,
    sifraProizvoda?: number,
    sifraPDV?: number
}
import { Body, Controller, Delete, Get, Param, Post ,Put} from '@nestjs/common';
import { Stavka } from './stavka';
import { StavkaService } from './stavka.service';

@Controller('stavka')
export class StavkaController {
    constructor(private readonly stavkaService: StavkaService){}

    @Get(':id')
    getAll(@Param('id') id: number){
        return this.stavkaService.getAll(id);
    }

    @Get(':id/:rb')
    getOne(@Param('id') id: number, @Param('rb')rb:number){
        console.log(`PARAMETRI SU ${id} i ${rb}`)
        return this.stavkaService.getOne(id,rb);
    }

    @Delete(':id/:rb')
    delete(@Param('id') id: number, @Param('rb')rb:number){
        return this.stavkaService.delete(id,rb);
    }

    @Post()
    insert(@Body() s: Stavka){
        return this.stavkaService.insertStavkaInterneOtpremnice(s);
    }

    @Put()
    update(@Body() s: Stavka){
        return this.stavkaService.updateStavkaInterneOtpremnice(s);
    }
}

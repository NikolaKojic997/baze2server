import { Test, TestingModule } from '@nestjs/testing';
import { StavkaService } from './stavka.service';

describe('StavkaService', () => {
  let service: StavkaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [StavkaService],
    }).compile();

    service = module.get<StavkaService>(StavkaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

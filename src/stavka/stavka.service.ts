import { Injectable } from '@nestjs/common';
import { dbBrocker } from 'src/db/db-brocker';
import { Stavka } from './stavka';

@Injectable()
export class StavkaService {
   async delete(id: number, rb: number) {
        await dbBrocker.getInstance().executeQuery(
            `delete from stavka_interne_otpremnice where SIFRA_OTPREMNICE = ${id} and REDNI_BROJ = ${rb}`
        );
    }

    async updateStavkaInterneOtpremnice(stavka: Stavka){
        return await dbBrocker.getInstance().executeQuery(`UPDATE STAVKA_INTERNE_OTPREMNICE
        set PODACI = PODACI_STAVKE_OTPREMNICE_TYPE(${stavka.rabat},${stavka.kolicina},${stavka.sifraPDV},${stavka.sifraProizvoda})
        WHERE SIFRA_OTPREMNICE = ${stavka.sifraOtpremnice} and REDNI_BROJ = ${stavka.redniBroj}   `)
    }
    async getAll(id: number): Promise<Stavka[]> {

        let res = await dbBrocker.getInstance().executeQuery(`select s.redni_broj,s.iznos_pdv, s.datum_dokumenta, s.podaci.daj_cenu(), s.podaci.daj_kolicinu(),s.podaci.CENA_STAVKE_PO_JEDINICI(),s.podaci.DAJ_RABAT(), s.podaci.UKUPNA_CENA_STAVKE(), p.naziv
        from stavka_interne_otpremnice s
        JOIN proizvod p on(s.podaci.sifra_proizvoda = p.sifra_proizvoda)
        WHERE s.SIFRA_OTPREMNICE = ${id}`);

        console.log(res);

        let arr: Stavka[] = [];
        for (let element of res.rows) {
            let s: Stavka = {
                sifraOtpremnice: id,
                redniBroj: element[0],
                pdv: element[1],
                datumDokumenta: new Date(element[2]),
                kolicina: element[4],
                cena: element[5],
                proizvod: element[8],
                rabat: element[6],
                ukupno: element[7]
            }
            arr.push(s);
        }
        return arr;
    }

    async getOne(sifraOtpremnice: number, redniBroj: number): Promise<Stavka> {

        let res = await dbBrocker.getInstance().executeQuery(`select s.redni_broj,s.iznos_pdv, s.datum_dokumenta, s.podaci.daj_cenu(), s.podaci.daj_kolicinu(),s.podaci.CENA_STAVKE_PO_JEDINICI(),s.podaci.DAJ_RABAT(), s.podaci.UKUPNA_CENA_STAVKE(), p.naziv
        from stavka_interne_otpremnice s
        JOIN proizvod p on(s.podaci.sifra_proizvoda = p.sifra_proizvoda)
        WHERE s.SIFRA_OTPREMNICE = ${sifraOtpremnice} and s.REDNI_BROJ = ${redniBroj}`);

        console.log(res);
        let s: Stavka = {
            sifraOtpremnice: sifraOtpremnice,
            redniBroj: res.rows[0][0],
            pdv: res.rows[0][1],
            datumDokumenta: new Date(res.rows[0][2]),
            kolicina: res.rows[0][4],
            cena: res.rows[0][5],
            proizvod: res.rows[0][8],
            rabat: res.rows[0][6],
            ukupno: res.rows[0][7]
        }

        return s;
    }
    public async insertStavkaInterneOtpremnice(stavka: Stavka) {
        
       
        return await dbBrocker.getInstance().executeQuery(`INSERT INTO STAVKA_INTERNE_OTPREMNICE
         (REDNI_BROJ, SIFRA_OTPREMNICE, PODACI, IZNOS_PDV, DATUM_DOKUMENTA)
         VALUES (${stavka.redniBroj}, ${stavka.sifraOtpremnice}, PODACI_STAVKE_OTPREMNICE_TYPE(${stavka.rabat},${stavka.kolicina},${stavka.sifraPDV},${stavka.sifraProizvoda}), 0, NULL)`);
    }
}

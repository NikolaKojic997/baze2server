import { Test, TestingModule } from '@nestjs/testing';
import { StavkaController } from './stavka.controller';

describe('StavkaController', () => {
  let controller: StavkaController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [StavkaController],
    }).compile();

    controller = module.get<StavkaController>(StavkaController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

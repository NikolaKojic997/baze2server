import { Module } from '@nestjs/common';
import { StavkaController } from './stavka.controller';
import { StavkaService } from './stavka.service';
@Module({
  imports: [],
  controllers: [StavkaController],
  providers: [StavkaService],
  exports: [StavkaService]
})
export class StavkaModule {}
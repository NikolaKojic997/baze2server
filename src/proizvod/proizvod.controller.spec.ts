import { Test, TestingModule } from '@nestjs/testing';
import { ProizvodController } from './proizvod.controller';

describe('ProizvodController', () => {
  let controller: ProizvodController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ProizvodController],
    }).compile();

    controller = module.get<ProizvodController>(ProizvodController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

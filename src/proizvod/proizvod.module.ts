import { Module } from '@nestjs/common';
import { ProizvodController } from './proizvod.controller';
import { ProizvodService } from './proizvod.service';

@Module({
  imports: [],
  controllers: [ProizvodController],
  providers: [ProizvodService],
  exports: [ProizvodService]
})
export class ProizvodModule {}

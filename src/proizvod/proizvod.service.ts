import { Injectable } from '@nestjs/common';
import { dbBrocker } from 'src/db/db-brocker';
import { Proizvod } from './proizvod';

@Injectable()
export class ProizvodService {
    async getAll():Promise<Proizvod[]>{
        let res = await dbBrocker.getInstance().executeQuery('SELECT * FROM PROIZVOD');
        let arr: Proizvod[] = [];
        for (let element of res.rows) {
            let p: Proizvod = {
                sifraProizvoda: element[0],
                imeProizvoda: element[1]
            }
            arr.push(p);
        }
        return arr;
    }
}

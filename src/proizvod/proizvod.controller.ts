import { Controller, Get } from '@nestjs/common';
import { ProizvodService } from './proizvod.service';

@Controller('proizvod')
export class ProizvodController {
    constructor(private readonly proizvodService: ProizvodService){}
    @Get()
    GetAll(){
        return this.proizvodService.getAll();
    }
}

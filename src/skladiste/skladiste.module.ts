import { Module } from '@nestjs/common';
import { SkladisteController } from './skladiste.controller';
import { SkladisteService } from './/skladiste.service';

@Module({
  imports: [],
  controllers: [SkladisteController],
  providers: [SkladisteService],
  exports: [SkladisteService]
})
export class SkladisteModule {}

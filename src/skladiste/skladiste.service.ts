import { Injectable } from '@nestjs/common';
import { dbBrocker } from 'src/db/db-brocker';
import { Skladiste } from './skladiste';

@Injectable()
export class SkladisteService {
    async getAllSkladista(): Promise<Skladiste[]> {
        let res = await dbBrocker.getInstance().executeQuery(
            "SELECT * FROM SKLADISTE"
        );
        let arr: Skladiste[] = [];
        for (let element of res.rows) {
            let s: Skladiste = {
                imeSkladista: element[1],
                sifraSkladista: element[0]
            };
            arr.push(s);
        }
        return arr;
    }

}

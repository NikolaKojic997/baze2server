import { Controller, Get } from '@nestjs/common';
import { SkladisteService } from './skladiste.service';

@Controller('skladiste')
export class SkladisteController {
    constructor(private readonly service: SkladisteService){}
    @Get()
    async getAllSkladista(){
        return await this.service.getAllSkladista();
    }
}

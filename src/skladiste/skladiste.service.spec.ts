import { Test, TestingModule } from '@nestjs/testing';
import { SkladisteService } from './skladiste.service';

describe('SkladisteService', () => {
  let service: SkladisteService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SkladisteService],
    }).compile();

    service = module.get<SkladisteService>(SkladisteService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

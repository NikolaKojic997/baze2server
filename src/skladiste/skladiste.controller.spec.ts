import { Test, TestingModule } from '@nestjs/testing';
import { SkladisteController } from './skladiste.controller';

describe('SkladisteController', () => {
  let controller: SkladisteController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SkladisteController],
    }).compile();

    controller = module.get<SkladisteController>(SkladisteController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

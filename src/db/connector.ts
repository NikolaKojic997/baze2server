import { getConnection, Connection, connectionClass } from "oracledb";

export class Connector {
  private static connection: Connection | any;

  private static async createConnection(): Promise<Connection | any> {
    const connection = await getConnection({
      user: "koja97",
      password: "admin123",
      connectString: "localhost:1521/xe",
    });
    return connection;
  }

  public static async getConnection(): Promise<Connection | any> {
    if (!this.connection) {
      this.connection = await this.createConnection();
    }
    return this.connection;
  }
}

import { Connection, connectionClass } from "oracledb";
// import { StavkaInterneOtpremnice } from "../domain/internaOtpremnica";
// import { Maloprodaja } from "../domain/maloprodaja";
// import { Skladiste } from "../domain/skladiste";
import { Connector } from "./connector";

export class dbBrocker {
  private static instance: dbBrocker;

  public static getInstance() {
    if (!this.instance) {
      this.instance = new dbBrocker();
    }
    return this.instance;
  }

  public async executeQuery(query: string): Promise<any> {
    let connection: Connection = await Connector.getConnection();
    try {
      let res = await connection.execute(query);
      await connection.commit();
      return res;
    } catch (err) {
      connection.rollback();
    }
  }

//   // Method should insert new Stavka. (set value of datumOtpreme from otpremnica);
//   public async insertStavkaInterneOtpremnice(stavka: StavkaInterneOtpremnice) {
//     return await this.executeQuery(`INSERT INTO STAVKA_INTERNE_OTPREMNICE
// (REDNI_BROJ, SIFRA_OTPREMNICE, PODACI, IZNOS_PDV, DATUM_DOKUMENTA)
// VALUES (${stavka.rednibroj}, ${stavka.sifraOtpremnice}, PODACI_STAVKE_OTPREMNICE_TYPE(${stavka.ruc},${stavka.kolicina},${stavka.sifraPDV},${stavka.sifraPDV}), 0, NULL)`);
//   }

//   public async getAllFromStavka(): Promise<StavkaInterneOtpremnice[]> {
//     let res = await this.executeQuery(
//       "SELECT * FROM STAVKA_INTERNE_OTPREMNICE"
//     );
//     let arrey: StavkaInterneOtpremnice[] = [];
//     for (let element of res.rows) {
//       let stavka: StavkaInterneOtpremnice = {
//         datumDokumenta: new Date(element[2]),
//         iznosPDV: element[1],
//         kolicina: element[3].KOLICINA,
//         rednibroj: element[4],
//         ruc: element[3].RABAT,
//         sifraOtpremnice: element[0],
//         sifraPDV: element[3].SIFRA_PDV,
//         sifraProizvoda: element[3].SIFRA_PROIZVODA,
//       };
//       arrey.push(stavka);
//     }
//     return arrey;
//   }

//   public async getOneStavka(
//     idStavka: number,
//     idOtpremnica: number
//   ): Promise<StavkaInterneOtpremnice> {
//     let res = await this.executeQuery(
//       `SELECT * FROM STAVKA_INTERNE_OTPREMNICE WHERE redni_broj = ${idStavka} AND SIFRA_OTPREMNICE = ${idOtpremnica}`
//     );
//     let element = res.rows[0];
//     let stavka: StavkaInterneOtpremnice = {
//       datumDokumenta: new Date(element[2]),
//       iznosPDV: element[1],
//       kolicina: element[3].KOLICINA,
//       rednibroj: element[4],
//       ruc: element[3].RABAT,
//       sifraOtpremnice: element[0],
//       sifraPDV: element[3].SIFRA_PDV,
//       sifraProizvoda: element[3].SIFRA_PROIZVODA,
//     };
//     return stavka;
//   }

//   public async deleteOneStavka(
//     idStavka: number,
//     idOtpremnica: number
//   ): Promise<boolean> {
//     let res = await this.executeQuery(
//       `DELETE FROM STAVKA_INTERNE_OTPREMNICE WHERE redni_broj = ${idStavka} AND SIFRA_OTPREMNICE = ${idOtpremnica}`
//     );
//     if (res.rowsAffected == 0) return false;
//     return true;
//   }

//   public async updateStavkaInterneOtpremnice(
//     stavka: StavkaInterneOtpremnice
//   ): Promise<boolean> {
//     let res = await this.executeQuery(`UPDATE STAVKA_INTERNE_OTPREMNICE
// SET  PODACI = PODACI_STAVKE_OTPREMNICE_TYPE(${stavka.ruc},${stavka.kolicina},${stavka.sifraPDV},${stavka.sifraPDV})
// WHERE redni_broj = ${stavka.rednibroj} AND SIFRA_OTPREMNICE = ${stavka.sifraOtpremnice} `);

//     if (res.rowsAffected == 0) return false;
//     return true;
//   }

//   public async getAllFromMaloprodaja(): Promise<Maloprodaja[]> {
//     let res = await this.executeQuery(
//       "SELECT * FROM MALOPRODAJA"
//     );
//     let arrey: Maloprodaja[] = [];
//     for (let element of res.rows) {
//       let m: Maloprodaja = {
//        imeMaloprodaje: res.IME_MALOPRODAJE,
//        sifraMaloprodaje: res.SIFRA_MALOPRODAJE
//       };
//       arrey.push(m);
//     }
//     return arrey;
//   }

//   public async getAllFromSkladiste(): Promise<Skladiste[]> {
//     let res = await this.executeQuery(
//       "SELECT * FROM SKLADISTE"
//     );
//     let arrey: Skladiste[] = [];
//     for (let element of res.rows) {
//       let s: Skladiste = {
//        imeSkladista: res.IME_SKLADISTA,
//        sifraSkladista: res.SIFRA_SKLADISTA
//       };
//       arrey.push(s);
//     }
//     return arrey;
//   }

}

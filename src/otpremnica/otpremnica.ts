export type Otpremnica = {
  sifraOtpremnice: number;
  datumDokumenta: Date;
  ukupno: number;  
  sifraSkladista: number;
  sifraMaloprodaje: number;
  sifraOtpreme: number;
  brojRadneKnjizice: number
}
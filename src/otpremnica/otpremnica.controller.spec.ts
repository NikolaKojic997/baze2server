import { Test, TestingModule } from '@nestjs/testing';
import { OtpremnicaController } from './otpremnica.controller';

describe('OtpremnicaController', () => {
  let controller: OtpremnicaController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [OtpremnicaController],
    }).compile();

    controller = module.get<OtpremnicaController>(OtpremnicaController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

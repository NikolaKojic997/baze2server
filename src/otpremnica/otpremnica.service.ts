import { Injectable } from '@nestjs/common';
import { dbBrocker } from 'src/db/db-brocker';
import { Otpremnica } from './otpremnica';
const date = require('date-and-time');


@Injectable()
export class OtpremnicaService {
    async deleteOne(id: number) {
       let res = await dbBrocker.getInstance().executeQuery(`DELETE FROM INTERNA_OTPREMNICA WHERE SIFRA_OTPREMNICE = ${id}`);
       console.log(res); 
    }
    async insertOtpremnica(o: Otpremnica) {
        let res = await dbBrocker.getInstance().executeQuery(`INSERT INTO INTERNA_OTPREMNICA
         (SIFRA_OTPREMNICE, SIFRA_OTPREME, BROJ_RADNE_KNJIZICE, DATUM_DOKUMENTA, UKUPNO, SIFRA_SKLADISTA, SIFRA_MALOPRODAJE)
         VALUES (${o.sifraOtpremnice}, ${o.sifraOtpreme}, 0,'${date.format(new Date(o.datumDokumenta), 'DD-MMM-YYYY')}', 0, ${o.sifraSkladista}, ${o.sifraMaloprodaje} )`);

        console.log(res);

        return res;

    }

    async updateOtpremnica(o: Otpremnica) {
        let res = await dbBrocker.getInstance().executeQuery(`UPDATE INTERNA_OTPREMNICA
         SET SIFRA_OTPREME = ${o.sifraOtpreme}, DATUM_DOKUMENTA ='${date.format(new Date(o.datumDokumenta), 'DD-MMM-YYYY')}', SIFRA_SKLADISTA = ${o.sifraSkladista}, SIFRA_MALOPRODAJE = ${o.sifraMaloprodaje} 
         WHERE SIFRA_OTPREMNICE = ${o.sifraOtpremnice}`)

        console.log(res);

        return res;

    }

    async getAll(): Promise<Otpremnica[]> {

        let res = await dbBrocker.getInstance().executeQuery("SELECT * FROM INTERNA_OTPREMNICA");

        console.log(res);

        let arrey: Otpremnica[] = [];
        for (let element of res.rows) {
            let o: Otpremnica = {
                sifraOtpremnice: element[0],
                brojRadneKnjizice: element[2],
                datumDokumenta: new Date(element[3]),
                sifraMaloprodaje: element[5],
                ukupno: element[4],
                sifraOtpreme: element[1],
                sifraSkladista: element[6]                
            }
            arrey.push(o);
        }
        return arrey;

    }

    async getOne(id : number): Promise<Otpremnica>{
        let res = await dbBrocker.getInstance().executeQuery(`SELECT * FROM INTERNA_OTPREMNICA WHERE SIFRA_OTPREMNICE = ${id}`);

        console.log(res);

            let o: Otpremnica = {
                sifraOtpremnice: res.rows[0][0],
                brojRadneKnjizice: res.rows[0][2],
                datumDokumenta: new Date(res.rows[0][3]),
                sifraMaloprodaje: res.rows[0][5],
                ukupno: res.rows[0][4],
                sifraOtpreme: res.rows[0][1],
                sifraSkladista: res.rows[0][6]                
            }
         
        return o;        
    }
}

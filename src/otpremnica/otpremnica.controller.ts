import { Body, Controller,Delete, Get, Param, Post, Put } from '@nestjs/common';
import { Otpremnica } from './otpremnica';
import { OtpremnicaService } from './otpremnica.service';

@Controller('otpremnica')
export class OtpremnicaController {
    constructor(private readonly otpremnicaService: OtpremnicaService){

    }
    @Get()
    async getAll(){
        return await this.otpremnicaService.getAll();
    }

    @Get(':id')
    async getOne(@Param('id') id: number){
        return await this.otpremnicaService.getOne(id);
    }
    @Post()
    async insertOtpremnica(@Body() o: Otpremnica){
        return await this.otpremnicaService.insertOtpremnica(o)
    }
    
    @Delete(':id')
    async deleteOne(@Param('id') id: number){
        console.log(id);
        return await this.otpremnicaService.deleteOne(id);
    }

    @Put()
    async update(@Body() o: Otpremnica){
        return await this.otpremnicaService.updateOtpremnica(o);
    }
}

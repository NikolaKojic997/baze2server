import { Module } from '@nestjs/common';
import { OtpremnicaController } from './otpremnica.controller';
import { OtpremnicaService } from './otpremnica.service';

@Module({
  imports: [],
  controllers: [OtpremnicaController],
  providers: [OtpremnicaService],
  exports: [OtpremnicaService]
})
export class OtpremnicaModule {}
